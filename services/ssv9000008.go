//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000008/constant"
	"git.forms.io/isaving/sv/ssv9000008/dao"
	constants "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
	"strconv"
	"time"
)

var Ssv9000008Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000008",
	ConfirmMethod: "ConfirmSsv9000008",
	CancelMethod:  "CancelSsv9000008",
}

type Ssv9000008 interface {
	TrySsv9000008(*models.SSV9000008I) (*models.SSV9000008O, error)
	ConfirmSsv9000008(*models.SSV9000008I) (*models.SSV9000008O, error)
	CancelSsv9000008(*models.SSV9000008I) (*models.SSV9000008O, error)
}

type Ssv9000008Impl struct {
	services.CommonTCCService
	Sv900008O                      *models.SSV9000008O
	Sv900008I                      *models.SSV9000008I
	O                              *xorm.Engine
	FezBusNm                       string
	UnfezFwNm                      int
	tevslcgp                       *dao.T_evslcgp
	FezStatus                      string
	QueryTevslcgpByFezMechType_2   *dao.T_evslcgp
	QueryTevslcgpByFType_2Records1 []dao.T_evslcgp
	Flag                           string
	//TODO ADD Service Self Define Field
}

// @Desc Ssv9000008 process
// @Author
// @Date 2020-12-12
func (impl *Ssv9000008Impl) TrySsv9000008(ssv9000008I *models.SSV9000008I) (ssv9000008O *models.SSV9000008O, err error) {

	//TODO Service Business Process,No Need Validate Paramer,Paramer Has Already Validate in Controller
	impl.Sv900008I = ssv9000008I
	impl.O = dao.EngineCache
	impl.Flag = "0"
	//冻结业务编号 自生成： 规则：25位 冻结状态（1位）+冻结发起方（1位）+DCN后四位（4位）+19位时间戳纳秒
	//Frozen business number, Generation rules:25-bit freeze status (1 bit) + freeze initiator (1 bit) + last four digits (4 digits) of DCN + 19-bit time stamp nanoseconds
	Dcn := impl.SrcAppProps[constants.SRCDCN]
	Dcn4 := Dcn[len(Dcn)-4:]
	UnixNano := strconv.FormatInt(time.Now().UnixNano(), 10)
	impl.FezBusNm = impl.Sv900008I.FezMode + impl.Sv900008I.FezMechType + Dcn4 + UnixNano

	//Add public data
	impl.tevslcgp = &dao.T_evslcgp{
		FezType:        impl.Sv900008I.FezMode, //0-正常;1-合约冻结/账户冻结;2-金额冻结- 3-暂禁
		EffctivDate:    impl.Sv900008I.EffctivDate,
		FezDueDate:     impl.Sv900008I.FezDueDate,
		FezDate:        time.Now().Format("2006-01-02"),
		FezTime:        time.Now().Format("15:04:05"),
		FezMech:        impl.Sv900008I.FezMech, //1-合约冻结/账户冻结;2-金额冻结- 3-暂禁
		FezEm:          impl.Sv900008I.FezEm,
		FezAuthEm:      impl.Sv900008I.FezAuthEm,
		FezMechType:    impl.Sv900008I.FezMechType,
		FezMechName:    impl.Sv900008I.FezMechName,
		FezMode:        impl.Sv900008I.FezMode,
		CstmrId:        impl.Sv900008I.CstmrId,
		CstmrTyp:       impl.Sv900008I.CstmrTyp,
		MdsTyp:         impl.Sv900008I.FezMdsTyp,
		MdsNm:          impl.Sv900008I.FezMdsNm,
		FezAgrmt:       impl.Sv900008I.FezAgrmt,
		FezAgrmtTyp:    impl.Sv900008I.FezAgrmtTyp,
		FezCur:         impl.Sv900008I.FezCur,
		FezAmt:         impl.Sv900008I.FezAmt,
		FezReason:      impl.Sv900008I.FezReason,
		FficialsName1:  impl.Sv900008I.FficialsName1,
		FficialsId1:    impl.Sv900008I.FficialsId1,
		FficialsName2:  impl.Sv900008I.FficialsName2,
		FficialsId2:    impl.Sv900008I.FficialsId2,
		BnkFezTpy:      impl.Sv900008I.BnkFezTpy,
		LastUpDatetime: time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:       impl.SrcAppProps[constants.TXDEPTCODE],
		LastUpEm:       impl.SrcAppProps[constants.TXUSERID],
		TccStatus:      1,
	}

	// 1-Account freeze，2-Amount freeze
	if impl.Sv900008I.FezMode == "2" {
		if impl.Sv900008I.CashTranFlag == "" || impl.Sv900008I.FezCur == "" || impl.Sv900008I.FezAmt == 0.0 {
			return nil, errors.New("FezCur, CashTranFlag, FezAmt cannot be empty", constant.ERRCODE5)
		}
	}
	//1-Authorities，2-Inside the bank
	if impl.Sv900008I.FezMechType == "1" {
		if impl.Sv900008I.FezDocNm == "" {
			return nil, errors.New("Frozen instrument number cannot be empty", constant.ERRCODE7)
		}
		if impl.Sv900008I.FficialsName1 == "" || impl.Sv900008I.FficialsId1 == "" {
			return nil, errors.New("Need input FficialsName1 and FficialsId1", constant.ERRCODE8)
		}

		//Qurey t_evslcgp where  FezAgrmt and FezMechType and  FezDocnm
		QueryTevslcgpRecord, err := dao.QueryTevslcgpByFezDocNm(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		if QueryTevslcgpRecord != nil {
			return nil, errors.New("The Frozen document number already exists", constant.ERRCODE9)
		}

		//Qurey t_evslcgp  where  FezAgrmt and FezMechType
		QueryTevslcgpByFezAgrmt, err := dao.QueryTevslcgpByFezAgrmt(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		//Unfreeze priority
		impl.UnfezFwNm = 1
		if QueryTevslcgpByFezAgrmt != nil {
			UnfezFwNmInt, _ := strconv.Atoi(QueryTevslcgpByFezAgrmt[0]["UnfezFwNm"])
			impl.UnfezFwNm = UnfezFwNmInt + 1
		}

		impl.FezStatus = "Y"
		//权力机构的账户冻结/暂禁
		if impl.Sv900008I.FezMode == "1" || impl.Sv900008I.FezMode == "3" {
			//判断当前冻结登记簿有没有权力机关未生效的记录，如果有，则本次不生效
			//合约号+合约类型+权力机关+未生效+未解冻
			QueryTevslcgpByMechTypeFezStatus, err := dao.QueryTevslcgpByFezStatusOrMechTypeFezSta(impl.Sv900008I, impl.Sv900008I.FezMechType, impl.O)
			if err != nil {
				return nil, err
			}
			if QueryTevslcgpByMechTypeFezStatus != nil {
				//有记录则本次不生效
				impl.FezStatus = "N"
			} else { //如果1没有记录，继续判断 权力机关账户冻结，暂禁 的记录
				//合约号+合约类型+权力机构+状态是1-账户冻结/3-暂禁
				QueryTevslcgpByFezType, err := dao.QueryTevslcgpByFezType(impl.Sv900008I, impl.O)
				if err != nil {
					return nil, err
				}
				if QueryTevslcgpByFezType != nil {
					//有记录则本次不生效
					impl.FezStatus = "N"
				} else { //如果2没有记录，则本次生效，更新个人合约表合约冻结状态为“账户冻结/暂禁”，如果原来有生效的银行账户冻结/暂禁记录，则更新为失效
					//合约号+合约类型+银行内部+生效+状态1-账户状态/3-暂禁查冻结信息表 有记录更新为无效，无记录不处理
					impl.QueryTevslcgpByFezMechType_2, err = dao.QueryTevslcgpByFezMechType_2(impl.Sv900008I, impl.O)
					if err != nil {
						return nil, err
					}
				}
			}
		}
		//权力机构的金额冻结
		if impl.Sv900008I.FezMode == "2" {
			//1.如果当前有权力机关账户冻结，暂禁 的记录
			//合约号+合约类型+权力机构+状态是1-账户冻结/3-暂禁
			QueryTevslcgpByFezType, err := dao.QueryTevslcgpByFezType(impl.Sv900008I, impl.O)
			if err != nil {
				return nil, err
			}
			if QueryTevslcgpByFezType != nil {
				//有记录则本次不生效
				impl.FezStatus = "N"
			} else { //如果1没有记录
				//2.如果当前有不生效的权力机构金额冻结，则本次金额冻结不生效
				//合约号+合约类型+权力机构+不生效+状态是2-金额冻结
				QueryTevslcgpByMechTypeFezSta_NFezM_2, err := dao.QueryTevslcgpByMechTypeFezSta_NFezM_2(impl.Sv900008I, impl.O)
				if err != nil {
					return nil, err
				}
				if QueryTevslcgpByMechTypeFezSta_NFezM_2 != nil {
					impl.FezStatus = "N"
				} else { //如果2没有记录
					//则判断当前账户当前余额是否大于等于本次冻结金额+累计冻结金额，如果大于等于，则本次冻结生效,如果小于
					if impl.Sv900008I.AmtCurrent < impl.Sv900008I.AmtFrozen+impl.Sv900008I.FezAmt {
						//逐笔按照优先级从低到高进行判断，把优先级最低并且生效的银行金额冻结置为失效，直到释放的金额满足本次权力机构金额冻结，同时把释放金额的银行冻结置为失效
						//合约号+生效+银行内部+状体是2-金额冻结 按解冻优先次序的降序排序查询冻结合约表，有记录，取第一条置无效并取冻结金额
						QueryTevslcgpByFType_2Records, err := dao.QueryTevslcgpByFezMechType_2FType_2(impl.Sv900008I, impl.O)
						if err != nil {
							return nil, err
						}
						if len(QueryTevslcgpByFType_2Records) == 0 {
							//如果没记录，则本次权力机构金额冻结不生效，同时需要判断是否存在银行端发起，并且生效的账户冻结或者暂禁，如果存在，则把合约状态置为金额冻结
							impl.FezStatus = "N"
							//合约号+合约类型+银行内部+生效+状态是1-账户冻结/3-暂禁查冻结信息表，如果有记录把合约状态设置为2-金额冻结
							impl.QueryTevslcgpByFezMechType_2, err = dao.QueryTevslcgpByFezMechType_2(impl.Sv900008I, impl.O)
							if err != nil {
								return nil, err
							}
						} else { //无记录/释放金额不满足，则本次无效且合约置为2-金额冻结，原有生效的银行金额冻结置失效 ，原有生效的银行账户冻结/暂禁不处理
							//逐笔按照优先级从低到高进行判断，把优先级最低并且生效的银行金额冻结置为失效，直到释放的金额满足本次权力机构金额冻结，同时把释放金额的银行冻结置为失效
							//需要释放的金额Release amount
							ReleaseAmt := (impl.Sv900008I.AmtFrozen + impl.Sv900008I.FezAmt) - impl.Sv900008I.AmtCurrent
							Amt := 0.0
							impl.QueryTevslcgpByFType_2Records1 = []dao.T_evslcgp{}
							for k, v := range QueryTevslcgpByFType_2Records {
								Amt = QueryTevslcgpByFType_2Records[k].FezAmt + Amt
								if Amt <= ReleaseAmt {
									impl.QueryTevslcgpByFType_2Records1 = append(impl.QueryTevslcgpByFType_2Records1, *v)
								} else {
									break
								}
							}
							if Amt >= ReleaseAmt { //当释放的金额>=冻结的金额，本次生效
								impl.FezStatus = "Y"
							} else { //当释放的金额<冻结的金额，本次失效
								impl.FezStatus = "N"
							}

						}
					} else {
						impl.FezStatus = "Y"
						//查询个人合约表，如果状态是0-正常-则改为为2-金额冻结，否则不修改
						//合约号
						QueryTpersonargeementById, err := dao.QueryTpersonargeementById(impl.Sv900008I, impl.O)
						if err != nil {
							return nil, err
						}
						if QueryTpersonargeementById == nil {
							return nil, errors.New("QueryTpersonargeementById Records not found", constant.ERRCODE3)
						} else {
							if QueryTpersonargeementById.FreezeType != "0" { //0-正常
								//加一个不更新个人合约表的标志
								impl.Flag = "1" //初始化为0，为1则不更新
							}
						}
					}
				}
			}
		}

		//Insert T_evslcgp One tcc=1
		impl.tevslcgp.FezBusNm = impl.FezBusNm
		impl.tevslcgp.FezDocnm = impl.Sv900008I.FezDocNm
		impl.tevslcgp.UnfezFwNm = impl.UnfezFwNm
		impl.tevslcgp.FezStatus = impl.FezStatus
		if err = dao.InsertTevslcgpOne(impl.tevslcgp, impl.O); err != nil {
			return nil, err
		}

		//Query t_person_argeement by Primary key
		QueryTpersonargeementById, err := dao.QueryTpersonargeementById(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		if QueryTpersonargeementById == nil {
			return nil, errors.New("QueryTpersonargeementById Records not found", constant.ERRCODE3)
		}

	} else if impl.Sv900008I.FezMechType == "2" { //2-Inside the bank
		if impl.Sv900008I.FezApNm == "" {
			return nil, errors.New("Frozen application number cannot be empty", constant.ERRCODE10)
		}

		//查询冻结信息表是否
		//合约号+合约类型+权力机构+未生效+状态暂禁/账户冻结，有记录则报错
		QueryTevslcgpByFezType_1N, err := dao.QueryTevslcgpByFezType_1N(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		if QueryTevslcgpByFezType_1N != nil {
			return nil, errors.New("There are already pending authority account freezes/temporary bans, not allowed", constant.ERRCODE12)
		}
		//Qurey t_evslcgp  where FezAgrmt and FezMechType and FezApNm
		QueryTevslcgpByFezApNm, err := dao.QueryTevslcgpByFezApNm(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		if QueryTevslcgpByFezApNm != nil {
			return nil, errors.New("The Frozen application number already exists", constant.ERRCODE11)
		}

		//Qurey t_evslcgp where  FezAgrmt and FezMechType
		QueryTevslcgpByFezAgrmt, err := dao.QueryTevslcgpByFezAgrmt(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		impl.UnfezFwNm = 1
		if QueryTevslcgpByFezAgrmt != nil {
			if QueryTevslcgpByFezAgrmt[0]["UnfezFwNm"] != "" {
				UnfezFwNmInt, _ := strconv.Atoi(QueryTevslcgpByFezAgrmt[0]["UnfezFwNm"])
				impl.UnfezFwNm = UnfezFwNmInt + 1
			}
		}

		impl.FezStatus = "Y"
		//1 - contract freeze / account freeze; 2 - amount freeze 3-prohibit
		if impl.Sv900008I.FezMode == "1" || impl.Sv900008I.FezMode == "3" {
			//判断当前冻结登记簿有没有未生效的记录，如果有，则本次不生效,且不用更新个人合约表
			//合约号+合约类型+未生效+未解冻 查冻结信息表
			QueryTevslcgpByFezStatus, err := dao.QueryTevslcgpByFezStatusOrMechTypeFezSta(impl.Sv900008I, "", impl.O)
			if err != nil {
				return nil, err
			}
			if QueryTevslcgpByFezStatus != nil {
				impl.FezStatus = "N"
			}
		}
		//Insert T_evslcgp One tcc=1
		impl.tevslcgp.FezBusNm = impl.FezBusNm
		impl.tevslcgp.FezDocnm = impl.Sv900008I.FezApNm
		impl.tevslcgp.UnfezFwNm = impl.UnfezFwNm
		impl.tevslcgp.FezStatus = impl.FezStatus
		if err = dao.InsertTevslcgpOne(impl.tevslcgp, impl.O); err != nil {
			return nil, err
		}

		//Query t_person_argeement  by Primary key
		QueryTpersonargeementById, err := dao.QueryTpersonargeementById(impl.Sv900008I, impl.O)
		if err != nil {
			return nil, err
		}
		if QueryTpersonargeementById == nil {
			return nil, errors.New("Records not found", constant.ERRCODE3)
		}

	} else {
		return nil, errors.New("FezMechType is wrong", constant.ERRCODE6)
	}

	ssv9000008O = &models.SSV9000008O{
		//TODO Assign  value to the OUTPUT Struct field
		FezBusNm:  impl.FezBusNm,
		FnfezFwNm: impl.UnfezFwNm,
	}

	return ssv9000008O, nil
}

func (impl *Ssv9000008Impl) ConfirmSsv9000008(ssv9000008I *models.SSV9000008I) (ssv9000008O *models.SSV9000008O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv9000008")
	impl.O = dao.EngineCache
	//update Tevslcgp tcc_status=0
	if err := dao.UpdateTevslcgpById(impl.FezBusNm, impl.O); err != nil {
		return nil, err
	}
	//银行内部的记录不生效不用更新个人合约表,同理权力机关不生效的记录也不用更新
	if impl.FezStatus != "N" {
		if impl.Flag != "1" {
			//Update t_person_argeement  freeze_type=FezMode
			if err := dao.UpdateTpersonargeementById(impl.Sv900008I.FezAgrmt, impl.Sv900008I.FezMode, impl.O); err != nil {
				return nil, err
			}
		}
	}

	if impl.Sv900008I.FezMode == "2" {
		//如果是权力机构金额冻结 有记录则更新个人合约表的状态为2-金额冻结
		if impl.QueryTevslcgpByFezMechType_2 != nil {
			if err := dao.UpdateTpersonargeementById(impl.Sv900008I.FezAgrmt, impl.Sv900008I.FezMode, impl.O); err != nil {
				return nil, err
			}
		}
	} else {
		//如果是权力机构的账户冻结/暂禁则  原来有生效的银行账户冻结/暂禁记录，则更新为失效
		if impl.QueryTevslcgpByFezMechType_2 != nil {
			//合约号+合约类型+银行内部+生效+状态1-账户状态/3-暂禁 更新为失效 因为已经查询出来可以取到对应的id，所以通过id就可以更新了
			if err := dao.UpdateTevslcgpByFezMechType_2FType_2(impl.QueryTevslcgpByFezMechType_2.FezBusNm, impl.O); err != nil {
				return nil, err
			}
		}
	}

	//合约号+生效+银行内部+状体是2-金额冻结 置无效 因为已经查询出来可以取到对应的id，所以通过id就可以更新了
	if len(impl.QueryTevslcgpByFType_2Records1) != 0 {
		for _, v := range impl.QueryTevslcgpByFType_2Records1 {
			if err := dao.UpdateTevslcgpByFezMechType_2FType_2(v.FezBusNm, impl.O); err != nil {
				return nil, err
			}
		}
	}
	return nil, nil
}

func (impl *Ssv9000008Impl) CancelSsv9000008(ssv9000008I *models.SSV9000008I) (ssv9000008O *models.SSV9000008O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv9000008")
	impl.O = dao.EngineCache
	//delete Tevslcgp tcc_status=1
	if err := dao.DeleteTevslcgpById(impl.FezBusNm, impl.O); err != nil {
		return nil, err
	}
	return nil, nil
}
