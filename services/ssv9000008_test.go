//Version: 1.0.0
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000008/dao"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestSSV9000008Impl_SSV9000008_Success_Qurey(t *testing.T) {
	input := &models.SSV9000008I{ //FezMechType=1 FezMode=2金额冻结
		FezMechType:   "1",
		FezMode:       "2",
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input6 := &models.SSV9000008I{ //FezMechType=1 FezMode=2金额冻结 如果1没有记录
		FezMechType:   "1",
		FezMode:       "2",
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input7 := &models.SSV9000008I{ //FezMechType=1 FezMode=3暂禁 如果1没有记录
		FezMechType:   "1",
		FezMode:       "3",
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input8 := &models.SSV9000008I{ //FezMechType=1 FezMode=2金额 如果1没有记录
		FezMechType:   "1",
		FezMode:       "2",
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input1 := &models.SSV9000008I{ //FezMechType=2 FezMode=2金额冻结
		FezMechType:   "2",
		FezMode:       "3",
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input3 := &models.SSV9000008I{
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezMode:       "2",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechType:   "3",
		FezMechName:   "22",
		FezDocNm:      "22",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input4 := &models.SSV9000008I{
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezMode:       "2",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechType:   "1",
		FezMechName:   "22",
		FezDocNm:      "",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "33",
		FficialsId1:   "33",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	input5 := &models.SSV9000008I{
		FezMdsNm:      "11",
		FezMdsTyp:     "11",
		FezAgrmt:      "11",
		FezAgrmtTyp:   "22",
		FezAccuntNme:  "22",
		FezMode:       "2",
		FezCur:        "22",
		CashTranFlag:  "22",
		FezMechType:   "1",
		FezMechName:   "22",
		FezDocNm:      "1",
		FezApNm:       "22",
		FezAmt:        10,
		FezReason:     "33",
		EffctivDate:   "33",
		FezDueDate:    "33",
		FficialsName1: "",
		FficialsId1:   "",
		FficialsName2: "33",
		FficialsId2:   "33",
		FezMech:       "33",
		FezEm:         "33",
		FezAuthEm:     "33",
		Remarks:       "33",
	}
	impl := Ssv9000008Impl{
		Sv900008I: input,
		O:         dao.EngineCache,
	}
	maps := make(map[string]string)
	maps["GlobalBizSeqNo"] = "10013"
	maps["SrcDcn"] = "C03101"
	impl.SrcAppProps = maps
	db, mock, _ := sqlmock.New()
	dao.EngineCache.DB().DB = db

	//FezMechType=1 FezMode=1账户冻结/3暂禁
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"})) //.AddRow("1000001", "1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"})) //.AddRow("1")
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"}).AddRow(1))
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_id"}).AddRow("2222"))
	_, _ = impl.TrySsv9000008(input7)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("t_person_argeement").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.ConfirmSsv9000008(input7)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.CancelSsv9000008(input7)

	//FezMechType=1 FezMode=2金额冻结  如果1没有记录
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"})) //.AddRow("1000001", "1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"}).AddRow(1))
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_id"}).AddRow("2222"))
	_, _ = impl.TrySsv9000008(input6)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("t_person_argeement").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.ConfirmSsv9000008(input6)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.CancelSsv9000008(input6)

	//FezMechType=1 FezMode=2金额冻结  QueryTevslcgpByFType_2Records无记录
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"})) //.AddRow("1000001", "1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"}))//.AddRow(1)
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_id"}).AddRow("2222"))
	_, _ = impl.TrySsv9000008(input8)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("t_person_argeement").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.ConfirmSsv9000008(input8)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.CancelSsv9000008(input8)
	//FezMechType=1 FezMode=2金额冻结
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"})) //.AddRow("1000001", "1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"UnfezFwNm"}).AddRow("1"))
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_id"}).AddRow("2222"))
	_, _ = impl.TrySsv9000008(input)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("t_person_argeement").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.ConfirmSsv9000008(input)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.CancelSsv9000008(input)

	//FezMechType=2  FezMode=3暂禁
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"})) //.AddRow("1000001", "1"))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"}))
	mock.ExpectQuery("t_evslcgp").WillReturnRows(sqlmock.NewRows([]string{"unfez_fw_nm"}).AddRow("1"))
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_id"}).AddRow("2222"))
	_, _ = impl.TrySsv9000008(input1)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("t_person_argeement").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.ConfirmSsv9000008(input1)
	mock.ExpectExec("t_evslcgp").WillReturnResult(sqlmock.NewResult(0, 1))
	_, _ = impl.CancelSsv9000008(input1)





	//FezMechType=3
	_, _ = impl.TrySsv9000008(input3)
	//FezMechType=1 FezDocNm=""
	_, _ = impl.TrySsv9000008(input4)
	//FezMechType=1 FficialsName1=""
	_, _ = impl.TrySsv9000008(input5)
}
