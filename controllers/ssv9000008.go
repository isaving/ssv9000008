//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000008/dao"
	"git.forms.io/isaving/sv/ssv9000008/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000008Controller struct {
	controllers.CommTCCController
}

func (*Ssv9000008Controller) ControllerName() string {
	return "Ssv9000008Controller"
}

// @Desc ssv9000008 controller
// @Description Entry
// @Param ssv9000008 body models.SSV9000008I true "body for user content"
// @Success 200 {object} models.SSV9000008O
// @router /ssv9000008 [post]
// @Author
// @Date 2020-12-12
func (c *Ssv9000008Controller) Ssv9000008() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000008Controller.Ssv9000008 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000008I := &models.SSV9000008I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000008I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000008I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000008 := &services.Ssv9000008Impl{}
	ssv9000008.New(c.CommTCCController)
	ssv9000008.Sv900008I = ssv9000008I
	ssv9000008.O = dao.EngineCache
	ssv9000008Compensable := services.Ssv9000008Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000008, ssv9000008Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000008I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000008O, ok := rsp.(*models.SSV9000008O); ok {
		if responseBody, err := models.PackResponse(ssv9000008O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv9000008 Controller
// @Description ssv9000008 controller
// @Param Ssv9000008 body models.SSV9000008I true body for SSV9000008 content
// @Success 200 {object} models.SSV9000008O
// @router /create [post]
/*func (c *Ssv9000008Controller) SWSsv9000008() {
	//Here is to generate API documentation, no need to implement methods
}
*/
