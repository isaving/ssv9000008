//Version: v0.01
package constant

//define error code
const (
	ERRCODE1  = "SVCM000001"
	ERRCODE2  = "SVCM000002"
	ERRCODE3  = "SV99000020" //Records not found
	ERRCODE4  = "SV77000004" //Duplicate primary key
	ERRCODE5  = "SV77000005" //FezCur, CashTranFlag, FezAmt cannot be empty
	ERRCODE6  = "SV77000006" //FezMechType is wrong
	ERRCODE7  = "SV77000007" //FezDocNm cannot be empty
	ERRCODE8  = "SV77000008" //Need input FficialsName1 and FficialsId1
	ERRCODE9  = "SV77000009" //The Frozen document number already exists
	ERRCODE10 = "SV77000010"//Frozen application number cannot be empty
	ERRCODE11 = "SV77000011"//The Frozen application number already exists
	ERRCODE12 = "SV77000026"//There are already pending authority account freezes/temporary bans, not allowed
	ERRCODE13 = "AC00000013"
	ERRCODE14 = "AC00000014"
	ERRCODE15 = "AC00000015"
	ERRCODE16 = "AC00000016"
	ERRCODE17 = "AC00000017"
	ERRCODE18 = "AC00000018"
	ERRCODE19 = "AC00000019"
	ERRCODE20 = "AC00000020"

	DLSUPCREATEDTS = "DlsupCreateDTS"
	DLSUUPDATEDTS  = "DlsuUpdateDTS"
)
const (
	DAACCAN1 = "DAACCAN1"
	SV000003 = "sv900003"
)
