//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000008I struct {
	FezMdsNm      string  `validate:"max=30" description:"Media number"`                          //介质号码
	FezMdsTyp     string  `validate:"max=3" description:"Media type"`                             //介质类型
	FezAgrmt      string  `validate:"required,max=30" description:"Freeze contract number"`       //冻结合约号
	FezAgrmtTyp   string  `validate:"required,max=5" description:"product code"`                  //冻结合约类型	10001-个人活期账户;10002-个人定期账户;10003-个人活期保证金账户;10004-个人定期保证金账户;10005-对公活期账户;10006-对公定期账户;10007-对公活期保证金账户;10008-对公定期保证金账户;10009-内部账户;20001-个人贷款账户;20002-对公贷款账户;30001-协议存款;30002-保证金;30003-同业存款;30004-智能通知存款;30005-法人账户透支;30006-集团账户;30007-协定存款;30008-收息账号;30009-活期存款转定期约定;30010-网银开户签约;30011-贷款;30012-贷款收费;30013-普通定期存款;30014-活期保证金存款;30015-定期保证金存款;30016-普通活期存款;40001-客户对账单;-
	FezAccuntNme  string  `validate:"max=200" description:"Freeze contract type"`                 //no冻结账户名称
	FezMode       string  `validate:"required,max=1" description:"Freezing method"`               //冻结方式  1-账户冻结，2-金额冻结 3-暂禁
	FezCur        string  `validate:"max=3" description:"Currency"`                               //币别  冻结方式为2-金额冻结时必输
	CashTranFlag  string  `validate:"max=2" description:"Money exchange logo"`                    //no 钞汇标志  冻结方式为2-金额冻结时必输
	FezMechType   string  `validate:"required,max=1" description:"Type of freezing organization"` //冻结机构类型  1-有权机关，2-银行内部
	FezMechName   string  `validate:"required" description:"Name of freezing organization"`       //冻结机构名称  如内部预授权冻结上送申请模块名称（如支付系统发起冻结
	FezDocNm      string  `description:"Frozen document number"`                                  //冻结文书号  冻结结构类型为1-有权机关时必输
	FezApNm       string  `description:"Frozen application  number"`                              //申请冻结编号  冻结结构类型为2-银行内部时必输
	FezAmt        float64 `description:"Whether to install the mark"`                             //冻结金额  冻结方式为2-金额冻结时必输
	FezReason     string  `description:"Frozen amount"`                                           //冻结原因
	EffctivDate   string  `validate:"required" description:"effective date"`                      //生效日期
	FezDueDate    string  `validate:"required" description:"Freeze expiry date"`                  //冻结到期日
	FficialsName1 string  `validate:"max=60" description:"Name of law enforcement officer one"`   //执法人员名称1
	FficialsId1   string  `validate:"max=60" description:"Law enforcement officer ID number one"` //执法人员证件号码1
	FficialsName2 string  `validate:"max=60" description:"Name of law enforcement officer two"`   //执法人员名称2
	FficialsId2   string  `validate:"max=60" description:"Law enforcement officer ID number two"` //执法人员证件号码2
	FezMech       string  `description:"Freeze agency"`                                           //冻结机构
	FezEm         string  `description:"Freeze teller"`                                           //冻结柜员
	FezAuthEm     string  `description:"Freeze authorized tellers"`                               //冻结授权柜员
	Remarks       string  `validate:"max=80" description:"Remarks"`                               // no 备注
	CstmrId       string  `description:"Frozen customer number"`                                  //冻结客户编号
	CstmrTyp      string  `description:"Frozen customer type"`                                    //冻结客户类型
	BnkFezTpy     string  `validate:"required,max=1" description:"Bank freeze distinction"`       //银行冻结区分 M-Internal management;T-tansation
	AmtFrozen     float64 `validate:"omitempty" description:"Frozen amount"`                      //冻结金额 cap 核算账户信息查询
	AmtCurrent    float64 `validate:"omitempty" description:"current balance"`                    //当前余额 cap 核算账户信息查询

}

type SSV9000008O struct {
	FezBusNm  string `validate:"required,max=30" description:"Frozen business number"` //冻结业务编号
	FnfezFwNm int    `validate:"len=6" description:"Unfreeze priority"`                //解冻优先次序
}

// @Desc Build request message
func (o *SSV9000008I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000008I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000008O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000008O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000008I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000008I) GetServiceKey() string {
	return "ssv9000008"
}
