package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUWB0I struct {
	//输入是个map
}

type DAACUWB0O struct {

}

type DAACUWB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUWB0I
}

type DAACUWB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUWB0O
}

type DAACUWB0RequestForm struct {
	Form []DAACUWB0IDataForm
}

type DAACUWB0ResponseForm struct {
	Form []DAACUWB0ODataForm
}

// @Desc Build request message
func (o *DAACUWB0RequestForm) PackRequest(DAACUWB0I DAACUWB0I) (responseBody []byte, err error) {

	requestForm := DAACUWB0RequestForm{
		Form: []DAACUWB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUWB0I",
				},
				FormData: DAACUWB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUWB0RequestForm) UnPackRequest(request []byte) (DAACUWB0I, error) {
	DAACUWB0I := DAACUWB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUWB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUWB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUWB0ResponseForm) PackResponse(DAACUWB0O DAACUWB0O) (responseBody []byte, err error) {
	responseForm := DAACUWB0ResponseForm{
		Form: []DAACUWB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUWB0O",
				},
				FormData: DAACUWB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUWB0ResponseForm) UnPackResponse(request []byte) (DAACUWB0O, error) {

	DAACUWB0O := DAACUWB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUWB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUWB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUWB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
