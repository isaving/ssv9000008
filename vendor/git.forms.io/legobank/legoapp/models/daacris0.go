package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRIS0I struct {
	BizFolnNo	string ` validate:"required,max=32"`
	SysFolnNo	string ` validate:"required,max=32"`
}

type DAACRIS0O struct {
	AcctgAcctNo   string  `json:"AcctgAcctNo"`
	BizFolnNo     string  `json:"BizFolnNo"`
	CalcBgnDate   string  `json:"CalcBgnDate"`
	CalcDays      int     `json:"CalcDays"`
	CalcEndData   string  `json:"CalcEndData"`
	CalcIntAmt    float64 `json:"CalcIntAmt"`
	CalcIntRate   int     `json:"CalcIntRate"`
	DealCode      string  `json:"DealCode"`
	IntAmt        float64 `json:"IntAmt"`
	IntType       string  `json:"IntType"`
	LastMaintBrno string  `json:"LastMaintBrno"`
	LastMaintDate string  `json:"LastMaintDate"`
	LastMaintTell string  `json:"LastMaintTell"`
	LastMaintTime string  `json:"LastMaintTime"`
	PmitRvrsFlg   string  `json:"PmitRvrsFlg"`
	RecordNo      int     `json:"RecordNo"`
	Status        string  `json:"Status"`
	SysFolnNo     string  `json:"SysFolnNo"`
	TccState      int     `json:"TccState"`
	WorkDate      string  `json:"WorkDate"`
}

type DAACRIS0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRIS0I
}

type DAACRIS0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRIS0O
}

type DAACRIS0RequestForm struct {
	Form []DAACRIS0IDataForm
}

type DAACRIS0ResponseForm struct {
	Form []DAACRIS0ODataForm
}

// @Desc Build request message
func (o *DAACRIS0RequestForm) PackRequest(DAACRIS0I DAACRIS0I) (responseBody []byte, err error) {

	requestForm := DAACRIS0RequestForm{
		Form: []DAACRIS0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRIS0I",
				},
				FormData: DAACRIS0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRIS0RequestForm) UnPackRequest(request []byte) (DAACRIS0I, error) {
	DAACRIS0I := DAACRIS0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRIS0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRIS0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRIS0ResponseForm) PackResponse(DAACRIS0O DAACRIS0O) (responseBody []byte, err error) {
	responseForm := DAACRIS0ResponseForm{
		Form: []DAACRIS0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRIS0O",
				},
				FormData: DAACRIS0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRIS0ResponseForm) UnPackResponse(request []byte) (DAACRIS0O, error) {

	DAACRIS0O := DAACRIS0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRIS0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRIS0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRIS0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
