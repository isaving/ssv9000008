package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020009I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"` //贷款核算账号
	QueryWay    string `valid:"Required;MaxSize(20)"` // 查询方式 1 全量查询 2 选择期数查询
	ArrRecCnt   int
	Records     []AC020009OPeriodNum
}
type AC020009OPeriodNum struct {
	PeriodNum		int64
}

type AC020009O struct {
	Records			[]interface{}
	AcctgAcctNo		string
	ArrRecCnt		int
}

type AC020009IDataForm struct {
	FormHead CommonFormHead
	FormData AC020009I
}

type AC020009ODataForm struct {
	FormHead CommonFormHead
	FormData AC020009O
}

type AC020009RequestForm struct {
	Form []AC020009IDataForm
}

type AC020009ResponseForm struct {
	Form []AC020009ODataForm
}

// @Desc Build request message
func (o *AC020009RequestForm) PackRequest(AC020009I AC020009I) (responseBody []byte, err error) {

	requestForm := AC020009RequestForm{
		Form: []AC020009IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020009I",
				},
				FormData: AC020009I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC020009RequestForm) UnPackRequest(request []byte) (AC020009I, error) {
	AC020009I := AC020009I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC020009I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020009I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC020009ResponseForm) PackResponse(AC020009O AC020009O) (responseBody []byte, err error) {
	responseForm := AC020009ResponseForm{
		Form: []AC020009ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020009O",
				},
				FormData: AC020009O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020009ResponseForm) UnPackResponse(request []byte) (AC020009O, error) {

	AC020009O := AC020009O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC020009O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020009O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC020009I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
