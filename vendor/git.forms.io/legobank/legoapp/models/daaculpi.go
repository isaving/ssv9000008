package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULPII struct {
	//输入是个map
}

type DAACULPIO struct {

}

type DAACULPIIDataForm struct {
	FormHead CommonFormHead
	FormData DAACULPII
}

type DAACULPIODataForm struct {
	FormHead CommonFormHead
	FormData DAACULPIO
}

type DAACULPIRequestForm struct {
	Form []DAACULPIIDataForm
}

type DAACULPIResponseForm struct {
	Form []DAACULPIODataForm
}

// @Desc Build request message
func (o *DAACULPIRequestForm) PackRequest(DAACULPII DAACULPII) (responseBody []byte, err error) {

	requestForm := DAACULPIRequestForm{
		Form: []DAACULPIIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULPII",
				},
				FormData: DAACULPII,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULPIRequestForm) UnPackRequest(request []byte) (DAACULPII, error) {
	DAACULPII := DAACULPII{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULPII, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULPII, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULPIResponseForm) PackResponse(DAACULPIO DAACULPIO) (responseBody []byte, err error) {
	responseForm := DAACULPIResponseForm{
		Form: []DAACULPIODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULPIO",
				},
				FormData: DAACULPIO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULPIResponseForm) UnPackResponse(request []byte) (DAACULPIO, error) {

	DAACULPIO := DAACULPIO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULPIO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULPIO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULPII) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
