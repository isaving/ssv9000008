package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAIL2001I struct {

}

type DAIL2001O struct {

}

type DAIL2001IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAIL2001ODataForm struct {
	FormHead CommonFormHead
	FormData DAIL2001O
}

type DAIL2001RequestForm struct {
	Form []DAIL2001IDataForm
}

type DAIL2001ResponseForm struct {
	Form []DAIL2001ODataForm
}

// @Desc Build request message
func (o *DAIL2001RequestForm) PackRequest(DAIL2001I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAIL2001RequestForm{
		Form: []DAIL2001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL2001I",
				},
				FormData: DAIL2001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAIL2001RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAIL2001I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAIL2001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL2001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAIL2001ResponseForm) PackResponse(DAIL2001O DAIL2001O) (responseBody []byte, err error) {
	responseForm := DAIL2001ResponseForm{
		Form: []DAIL2001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL2001O",
				},
				FormData: DAIL2001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAIL2001ResponseForm) UnPackResponse(request []byte) (DAIL2001O, error) {

	DAIL2001O := DAIL2001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAIL2001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL2001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAIL2001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
