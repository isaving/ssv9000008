package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type CM090004I struct {
	OrgId              string	//机构号
	Status             string	//状态
	OrgType            string	//机构类型
	OrgLvl             string	//机构级别
	BillStatus         string	//扎帐状态
	LegalPersonCode    string	//法人代码
	SupOrgId           string	//级机构号
	AcctingOrgId       string	//核算机构号
	LiquidatOrgId      string	//清算机构号
	WithdrawalOwnOrgId string	//撤并后归属机构号
	PaymentSysLineNo   string	//支付系统行号
	SwiftCode          string	//SWIFT代码
	OrgName            string	//名称
	Abbreviation       string	//(简称)
	Addr               string	//地址
	Phone              string	//电话
	//OpenDate           string	//开业日期
	//ClosingDate        string	//结业日期
	//EffDate            string	//生效日期
	//ExpDate            string	//失效日期
	CreateOrgId        string	//创建机构
	CreateTeller       string	//创建柜员
	LastMaintDate      string	//最后更新日期
	LastMaintTime      string	//最后更新时间
	LastMaintBrno      string	//最后更新机构
	LastMaintTell      string	//最后更新柜员
}

type CM090004O struct {
	Status	string
	OrgId	string
}

// @Desc Build request message
func (o *CM090004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *CM090004I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *CM090004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *CM090004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *CM090004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

