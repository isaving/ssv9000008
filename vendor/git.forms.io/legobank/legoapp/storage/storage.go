package storage

import (
	"encoding/csv"
	"fmt"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"io"
	"os"
	"path/filepath"
)

type StorageContext struct {
	Host              string
	Bucket            string
	AccessKey         string
	SecretKey         string
	MinUploadPartSize int64
	S3Client          *s3.S3
	Uploader          *s3manager.Uploader
	File              *os.File
}

type StorageInterface interface {
	DOSInitS3Client(endpoint, bucket, accessKey, secretKey string, minUploadPartSize int64) error
	DOSPutObject(filePath, key string) (*s3manager.UploadOutput, error)
	DOSPutObjectFromReader(file io.Reader, key string) (*s3manager.UploadOutput, error)
	DOSDownloadObject(key string) (*s3.GetObjectOutput, error)
	DOSDeleteObject(fileId string) (*s3.DeleteObjectOutput, error)

	FileOpenFile(name string, flag int, perm os.FileMode) error
	FileWriteString(context string) (n int, err error)
	FileWrite(context []byte) (n int, err error)
	CSVFileWriteAll(records [][]string) (err error)
	FileSync() error
	FileClose() error
	FileRemove(name string) error
	FileStat(name string) (os.FileInfo, error)
	FileRead(buffer []byte) (n int, err error)
	FilepathWalk(root string, walkFn filepath.WalkFunc) error
}

func NewStorageInterfaceImpl() StorageInterface {
	return &StorageInterfaceImpl{
		Context: &StorageContext{},
	}
}

type StorageInterfaceImpl struct {
	Context *StorageContext
}

func (s *StorageInterfaceImpl) DOSInitS3Client(endpoint, bucket, accessKey, secretKey string, minUploadPartSize int64) (err error) {
	if minUploadPartSize <= 0 {
		minUploadPartSize = 10 * 1024 * 1024
	}

	s.Context.Host = endpoint
	s.Context.Bucket = bucket
	s.Context.AccessKey = accessKey
	s.Context.SecretKey = secretKey
	s.Context.MinUploadPartSize = minUploadPartSize

	s3Config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(s.Context.AccessKey, s.Context.SecretKey, ""),
		Endpoint:    aws.String(s.Context.Host),
		Region:      aws.String("default"),
		DisableSSL:  aws.Bool(true),
		//if true ,use path-style addressing: http://s3.amazonaws.com/BUCKET/KEY
		//if false,use virtual hosted bucket addressing: http://BUCKET.s3.amazonaws.com/KEY
		S3ForcePathStyle: aws.Bool(true),
	}
	newSession, err := session.NewSession(s3Config)
	if err != nil {
		return err
	}
	s.Context.S3Client = s3.New(newSession)
	s.Context.Uploader = s3manager.NewUploader(newSession)

	return nil
}

func (s *StorageInterfaceImpl) DOSPutObject(filePath, key string) (*s3manager.UploadOutput, error) {
	if nil == s.Context.S3Client {
		return nil, fmt.Errorf("StorageContext(for DOS) hasn't been initialized, please use DOSInitS3Client to initialize the StorageContext")
	}
	diskFile, err := os.Open(filePath)
	if err != nil {
		log.Errorf("open file failed,path=%s,err=%v", filePath, err)
		return nil, err
	}

	return s.Context.Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.Context.Bucket),
		Key:    aws.String(key),
		Body:   diskFile,
	}, func(u *s3manager.Uploader) {
		u.PartSize = s.Context.MinUploadPartSize
	})
}

func (s *StorageInterfaceImpl) DOSPutObjectFromReader(file io.Reader, key string) (*s3manager.UploadOutput, error) {
	if nil == s.Context.S3Client {
		return nil, fmt.Errorf("StorageContext(for DOS) hasn't been initialized, please use DOSInitS3Client to initialize the StorageContext")
	}
	return s.Context.Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.Context.Bucket),
		Key:    aws.String(key),
		Body:   file,
	})
}

func (s *StorageInterfaceImpl) DOSDownloadObject(key string) (*s3.GetObjectOutput, error) {
	if nil == s.Context.S3Client {
		return nil, fmt.Errorf("StorageContext(for DOS) hasn't been initialized, please use DOSInitS3Client to initialize the StorageContext")
	}

	input := &s3.GetObjectInput{
		Bucket: aws.String(s.Context.Bucket),
		Key:    aws.String(key),
	}

	return s.Context.S3Client.GetObject(input)
}

func (s *StorageInterfaceImpl) DOSDeleteObject(fileId string) (*s3.DeleteObjectOutput, error) {
	if nil == s.Context.S3Client {
		return nil, fmt.Errorf("StorageContext(for DOS) hasn't been initialized, please use DOSInitS3Client to initialize the StorageContext")
	}

	return s.Context.S3Client.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(s.Context.Bucket),
		Key:    aws.String(fileId),
	})
}

func (s *StorageInterfaceImpl) FileOpenFile(name string, flag int, perm os.FileMode) error {
	if file, err := os.OpenFile(name, flag, perm); nil != err {
		return err
	} else {
		s.Context.File = file
	}

	return nil
}

func (s *StorageInterfaceImpl) FileWriteString(context string) (n int, err error) {
	if nil == s.Context.File {
		return 0, fmt.Errorf("StorageContext(for File) hasn't been initialized, please use FileOpenFile to initialize the StorageContext")
	}

	return s.Context.File.WriteString(context)
}

func (s *StorageInterfaceImpl) FileWrite(context []byte) (n int, err error) {
	if nil == s.Context.File {
		return 0, fmt.Errorf("StorageContext(for File) hasn't been initialized, please use FileOpenFile to initialize the StorageContext")
	}

	return s.Context.File.Write(context)
}

func (s *StorageInterfaceImpl) CSVFileWriteAll(records [][]string) (err error) {
	if nil == s.Context.File {
		return fmt.Errorf("StorageContext(for File) hasn't been initialized, please use FileOpenFile to initialize the StorageContext")
	}
	s.Context.File.WriteString("\xEF\xBB\xBF") // UTF-8 BOM
	w := csv.NewWriter(s.Context.File)
	w.WriteAll(records)
	w.Flush()

	return nil
}

func (s *StorageInterfaceImpl) FileSync() error {
	if nil == s.Context.File {
		return fmt.Errorf("StorageContext(for File) hasn't been initialized, please use FileOpenFile to initialize the StorageContext")
	}
	return s.Context.File.Sync()
}

func (s *StorageInterfaceImpl) FileClose() error {
	if nil == s.Context.File {
		return fmt.Errorf("StorageContext(for File) hasn't been initialized, please use FileOpenFile to initialize the StorageContext")
	}
	return s.Context.File.Close()
}

func (s *StorageInterfaceImpl) FileRemove(name string) error {
	return os.Remove(name)
}

func (s *StorageInterfaceImpl) FileStat(name string) (os.FileInfo, error) {
	return os.Stat(name)
}

func (s *StorageInterfaceImpl) FileRead(buffer []byte) (n int, err error) {
	return s.Context.File.Read(buffer)
}

func (s *StorageInterfaceImpl) FilepathWalk(root string, walkFn filepath.WalkFunc) error {
	return filepath.Walk(root, walkFn)
}
