//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"fmt"
	log "git.forms.io/universe/comm-agent/common/log"
	proto "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/common/serializer"
	"github.com/go-errors/errors"
	"github.com/valyala/fasthttp"
	"net/http"
	"time"
)

// null pointer error raising when input parameter is nil
// The caller can use this variable to check if the error is excepted error.
var ErrNullPointer = errors.New("Null pointer error")

var (
	// Global http client object
	client = &fasthttp.Client{
		// Default value of MaxConnsPerHost is 512, increase to 16384
		MaxConnsPerHost: 16384,
		// Disable idempotent calls attempts when remote call abnormal.
		//
		// default max attempts is 5 times.
		MaxIdemponentCallAttempts: 1,
	}
)

// postRequest is a internal function for client post request to server
// and it's returns responses where from server endpoint
func postRequest(request string, path string, isNeedReply bool) (string, error) {
	var fullUrl string
	//if config.CommAgentCfg.IsConnectWithUnixSocket() {
	//	fullUrl = unixSocketHttpAddr + path
	//} else {
	fullUrl = commServerAddr + path
	//}

	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)

	req.Header.SetMethod(http.MethodPost)

	req.SetRequestURI(fullUrl)
	req.SetBody([]byte(request))

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	client.ReadTimeout = time.Second * 30
	client.WriteTimeout = time.Second * 30

	if err := client.DoTimeout(req, resp, 30*time.Second); err != nil {
		log.Errorf("post to server failed err=%v", err)
		return "", errors.Wrap(err, 0)
	}

	if resp.StatusCode() != fasthttp.StatusOK {
		//return "", fmt.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, resp.StatusCode())
		return "", errors.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, resp.StatusCode())
	}
	if isNeedReply {
		body := resp.Body()
		responseString := string(body)

		return responseString, nil
	} else {
		return "", nil
	}
}

// postRequestV2 is a internal function for client post request to server
// and it's returns responses where from server endpoint
// allow the caller to specify a timeout(millisecond)
func postRequestV2(request string, path string, isNeedReply bool, timeoutMillisecond int) (string, error) {
	var fullUrl string
	fullUrl = commServerAddr + path

	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)

	req.Header.SetMethod(http.MethodPost)

	req.SetRequestURI(fullUrl)
	req.SetBody([]byte(request))

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	client.ReadTimeout = time.Millisecond * time.Duration(timeoutMillisecond)
	client.WriteTimeout = time.Millisecond * time.Duration(timeoutMillisecond)

	if err := client.DoTimeout(req, resp, time.Duration(timeoutMillisecond)*time.Millisecond); err != nil {
		log.Errorf("post to server failed err=%v", err)
		return "", errors.Wrap(err, 0)
	}

	if resp.StatusCode() != fasthttp.StatusOK {
		//return "", fmt.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, resp.StatusCode())
		return "", errors.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, resp.StatusCode())
	}
	if isNeedReply {
		body := resp.Body()
		responseString := string(body)

		return responseString, nil
	} else {
		return "", nil
	}
}

// SendRequestMessage publishes a request event to the event mesh and waiting response until server reply.
func SendRequestMessage(msg *UserMessage) (*UserMessage, error) {
	if nil == msg {
		log.Error("SendRequestMessage msg=nil")
		return nil, ErrNullPointer
	}

	protoMsg := UserMsgToProtocolMsg(msg)
	requestString := serializer.ProtoMsg2String(&protoMsg, nil)

	responseString, err := postRequest(requestString, SEND_REQUEST_REPLY_MSG_PATH, true)
	if nil != err {
		log.Errorf("SendRequestMessage.postRequest failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return nil, err
	}

	retProtoMsg, err := serializer.String2protoMsg(responseString)
	if nil != err {
		log.Errorf("SendRequestMessage.String2protoMsg failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return nil, err
	}

	retUserMessage := ProtocolMsgToUserMsg(&retProtoMsg)

	return &retUserMessage, nil
}

// SendRequestMessageV2 publishes a request event to the event mesh and waiting response until server reply.
// this version is allow callers specify a timout(millisecond)
func SendRequestMessageV2(msg *UserMessage, timeoutMilliseconds int) (*UserMessage, error) {
	if nil == msg {
		log.Error("SendRequestMessage msg=nil")
		return nil, ErrNullPointer
	}

	protoMsg := UserMsgToProtocolMsg(msg)
	requestString := serializer.ProtoMsg2String(&protoMsg, nil)

	responseString, err := postRequestV2(requestString, SEND_REQUEST_REPLY_MSG_PATH, true, timeoutMilliseconds)
	if nil != err {
		log.Errorf("SendRequestMessageV2.postRequest failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return nil, err
	}

	retProtoMsg, err := serializer.String2protoMsg(responseString)
	if nil != err {
		log.Errorf("SendRequestMessageV2.String2protoMsg failed:[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return nil, err
	}

	retUserMessage := ProtocolMsgToUserMsg(&retProtoMsg)

	return &retUserMessage, nil
}

//ReplySemiSyncCall is use for requester initiate a semi synchronized call.
func ReplySemiSyncCall(msg *UserMessage) (err error) {
	if nil == msg {
		log.Error("ReplySemiSyncCall msg=nil")
		return ErrNullPointer
	}
	log.Debugf("ReplySemiSyncCall, msg=[%++v]", msg)
	protoMsg := UserMsgToProtocolMsg(msg)
	requestString := serializer.ProtoMsg2String(&protoMsg, nil)
	responseString, err := postRequest(requestString, REPLY_SEMI_SYNC_CALL_PATH, true)
	if nil != err {
		log.Errorf("ReplySemiSyncCall.postRequest failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return err
	}

	if err := serializer.String2Error(responseString); nil != err {
		log.Errorf("ReplySemiSyncCall.String2Error failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return err
	}

	return nil
}

//PublishMessage is use for publish a event to solace
func PublishMessage(msg *UserMessage) (err error) {
	if nil == msg {
		log.Error("PublishMessage msg=nil")
		return ErrNullPointer
	}
	log.Debugf("publish message, msg.TopicAttribute=[%++v],msg.AppProps=[%++v],msg.Body=[%s]",
		msg.TopicAttribute, msg.AppProps, string(msg.Body))

	protoMsg := UserMsgToProtocolMsg(msg)
	requestString := serializer.ProtoMsg2String(&protoMsg, nil)
	responseString, err := postRequest(requestString, SEND_TOPIC_MSG_PATH, true)
	if nil != err {
		log.Errorf("PublishMessage.postRequest failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return err
	}

	if err := serializer.String2Error(responseString); nil != err {
		log.Errorf("PublishMessage.String2Error failed, error=[%v], topic attribute=[%++v]", err, msg.TopicAttribute)
		return err
	}

	return nil
}

func SendAckToQueueMessage(ackMsgId uint64) error {
	var protoMsg proto.Message
	protoMsg.Id = ackMsgId
	requestString := serializer.ProtoMsg2String(&protoMsg, nil)
	responseString, err := postRequest(requestString, SEND_QUEUE_ACK_PATH, true)
	if nil != err {
		log.Errorf("SendAckToQueueMessage.postRequest failed, error=[%v]", err)
	}

	if err := serializer.String2Error(responseString); nil != err {
		log.Errorf("SendAckToQueueMessage.String2Error failed, error=[%v]", err)
		return err
	}

	return nil
}

// BuildOPSTopicAttributes is build topic attributes of OPS request events
func BuildOPSTopicAttributes(dstOrg, dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)

	if dstTopicId == "" {
		log.Errorf("topic id is empty, please check!")
		return attributeMap, fmt.Errorf("dstOrg=%s,dstTopicId=%s invalid", dstOrg, dstTopicId)
	}

	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_UNIVERSE
	attributeMap[proto.TOPIC_DESTINATION_ORG] = dstOrg
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// BuildBussinessTopicAttributes is build topic attributes of business service request events
func BuildBussinessTopicAttributes(dstOrg, dstDcn, dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	if dstTopicId == "" {
		log.Errorf("topic id is empty, please check!")
		return attributeMap, fmt.Errorf("topic id is empty, please check")
	}

	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_TRANSACTION
	attributeMap[proto.TOPIC_DESTINATION_ORG] = dstOrg
	attributeMap[proto.TOPIC_DESTINATION_DCN] = dstDcn
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// BuildMetricTopicAttributes is build topic attributes of UserMessage for metric collection events
func BuildMetricTopicAttributes(dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_METRICS
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// BuildRequestSpecFileTopicAttributes is build topic attributes of UserMessage for request specification file events.
func BuildRequestSpecFileTopicAttributes(dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_UNIVERSE
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// BuildLogTopicAttributes is build topic attributes of UserMessage for log events.
func BuildLogTopicAttributes(dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_LOG
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// BuildHealthAttributes is build topic attributes of UserMessage for health check events.
func BuildHealthAttributes(dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	attributeMap[proto.TOPIC_TYPE] = proto.TOPIC_TYPE_HEARTBEAT
	attributeMap[proto.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}
