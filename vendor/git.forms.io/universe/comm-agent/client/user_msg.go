//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"encoding/base64"
	proto "git.forms.io/universe/comm-agent/common/protocol"
)

// UserMessage is data struct of service and comm-agent server
type UserMessage struct {

	// Each interaction has a session unique identifier
	Id uint64

	// Used to save topic-related key-values pairs
	TopicAttribute map[string]string

	// True means this is a synchronous call message
	// False means this is an asynchronous call message
	NeedReply bool

	// If it's a synchronous call message, this field value is true
	// If it's a asynchronous call message, this field value is false
	NeedAck bool

	// sessionName is used to distinguish which session the message was sent to solace
	SessionName string

	// app properties
	// service can set this attributes pass to target service
	AppProps map[string]string

	// message payload
	Body []byte
}

// GetMsgTopicType is get topic type of message
func (msg *UserMessage) GetMsgTopicType() string {
	return msg.TopicAttribute[proto.TOPIC_TYPE]
}

// GetMsgTopicId is get topic id of message
func (msg *UserMessage) GetMsgTopicId() string {
	return msg.TopicAttribute[proto.TOPIC_ID]
}

// UserMsgToProtocolMsg is convert user message to proto message
func UserMsgToProtocolMsg(userMsg *UserMessage) proto.Message {
	protoMsg := proto.Message{
		AppProps:       userMsg.AppProps,
		Id:             userMsg.Id,
		TopicAttribute: userMsg.TopicAttribute,
		SessionName:    userMsg.SessionName,
	}

	// set session name to "default" if not specified
	if userMsg.SessionName == "" {
		protoMsg.SessionName = "default"
	}

	// 0: means the delivery mode is direct
	// 1: means the delivery mode is persistent
	// set delivery mode to "direct" if not specified
	if nil != userMsg.AppProps && "1" == userMsg.AppProps[proto.DELIVERY_MODE] {
		protoMsg.DeliveryMode = 1
	} else {
		protoMsg.DeliveryMode = 0
	}

	// encode the message format to base64
	if len(userMsg.Body) > 0 {
		protoMsg.Body = base64.StdEncoding.EncodeToString(userMsg.Body)
	}

	return protoMsg
}

// ProtocolMsgToUserMsg is convert proto message to user message
func ProtocolMsgToUserMsg(protoMsg *proto.Message) UserMessage {
	userMsg := UserMessage{
		AppProps:       protoMsg.AppProps,
		TopicAttribute: protoMsg.TopicAttribute,
		Id:             protoMsg.Id,
		NeedReply:      protoMsg.NeedReply,
		NeedAck:        protoMsg.NeedAck,
	}

	// decode base64 message
	if len(protoMsg.Body) > 0 {
		userMsg.Body, _ = base64.StdEncoding.DecodeString(protoMsg.Body)
	}

	return userMsg
}
