//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package event

import (
	"git.forms.io/universe/common/event_handler/register"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/config"
)

// @Desc bind the method to the topicID
func InitEventRouters() error {
	eventRouterRegister := register.NewEventHandlerRegister()
	eventRouterRegister.Router(config.CmpSvrConfig.DtsClientConfirmTopicId, &controllers.DTSCallbackController{}, "AtomicTransactionConfirm")
	eventRouterRegister.Router(config.CmpSvrConfig.DtsClientCancelTopicId, &controllers.DTSCallbackController{}, "AtomicTransactionCancel")
	return nil
}
