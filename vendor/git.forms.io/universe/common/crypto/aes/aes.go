//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"git.forms.io/universe/common/crypto/mode"
	"git.forms.io/universe/common/crypto/padding"
	"github.com/go-errors/errors"
)

type AES struct {
	padding string
	mode    string
	key     []byte
}

func NewAES(padding string, md string, key []byte) *AES {
	return &AES{padding: padding, mode: md, key: key}
}

func (a *AES) Encrypt(plainText []byte) ([]byte, error) {
	block, err := aes.NewCipher(a.key)
	if nil != err {
		return nil, errors.Wrap(err, 0)
	}

	plainText, err = padding.Padding(plainText, block.BlockSize(), a.padding)
	if nil != err {
		return nil, err
	}
	var ivOrNonce []byte

	if "ECB" != a.mode {
		ivOrNonce, err = padding.GenerateIVOrNonce("AES", a.mode)
		if nil != err {
			return nil, err
		}
	}

	switch a.mode {
	case "CBC":
		{
			blockMode := cipher.NewCBCEncrypter(block, ivOrNonce)
			blockMode.CryptBlocks(plainText, plainText)

			return append(ivOrNonce, plainText...), nil
		}
	case "CFB":
		{
			blockMode := cipher.NewCFBEncrypter(block, ivOrNonce)
			blockMode.XORKeyStream(plainText, plainText)

			return append(ivOrNonce, plainText...), nil
		}
	case "CTR":
		{
			blockMode := cipher.NewCTR(block, ivOrNonce)
			blockMode.XORKeyStream(plainText, plainText)

			return append(ivOrNonce, plainText...), nil
		}
	case "ECB":
		{
			blockMode := mode.NewECBEncrypter(block)
			encrypted := make([]byte, len(plainText))
			blockMode.CryptBlocks(encrypted, plainText)

			return encrypted, nil
		}
	case "OFB":
		{
			blockMode := cipher.NewOFB(block, ivOrNonce)
			blockMode.XORKeyStream(plainText, plainText)

			return append(ivOrNonce, plainText...), nil
		}
	case "GCM":
		{
			aesgcm, err := cipher.NewGCM(block)

			if err != nil {
				return nil, err
			}

			// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
			//nonce := make([]byte, 12)
			//if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
			//	return nil, err
			//}

			cipherText := aesgcm.Seal(nil, ivOrNonce, plainText, nil)

			return append(ivOrNonce, cipherText...), nil

		}
	}
	return nil, nil
}

func (a *AES) Decrypt(preCipherText []byte) ([]byte, error) {
	block, err := aes.NewCipher(a.key)
	if nil != err {
		return nil, errors.Wrap(err, 0)
	}

	var ivOrNonce []byte
	var cipherText []byte
	if "ECB" != a.mode {
		if "GCM" == a.mode && len(preCipherText) < padding.LENGTH_OF_AES256_GCM_NONCE {

			return nil, errors.Errorf("Invalid length of cipherText, cipherText=[%s]", string(preCipherText))
		} else if len(preCipherText) < padding.LENGTH_OF_AES_COMMON_IV {

			return nil, errors.Errorf("Invalid length of cipherText, cipherText=[%s]", string(preCipherText))
		} else if "GCM" == a.mode {
			ivOrNonce = preCipherText[0:padding.LENGTH_OF_AES256_GCM_NONCE]
			cipherText = preCipherText[padding.LENGTH_OF_AES256_GCM_NONCE:]
		} else {
			ivOrNonce = preCipherText[0:padding.LENGTH_OF_AES_COMMON_IV]
			cipherText = preCipherText[padding.LENGTH_OF_AES_COMMON_IV:]
		}
	} else {
		cipherText = preCipherText
	}

	switch a.mode {
	case "CBC":
		{
			blockMode := cipher.NewCBCDecrypter(block, ivOrNonce)
			blockMode.CryptBlocks(cipherText, cipherText)
		}
	case "CFB":
		{
			blockMode := cipher.NewCFBDecrypter(block, ivOrNonce)
			blockMode.XORKeyStream(cipherText, cipherText)
		}
	case "CTR":
		{
			blockMode := cipher.NewCTR(block, ivOrNonce)
			blockMode.XORKeyStream(cipherText, cipherText)
		}
	case "ECB":
		{
			blockMode := mode.NewECBDecrypter(block)
			blockMode.CryptBlocks(cipherText, cipherText)
		}
	case "OFB":
		{
			blockMode := cipher.NewOFB(block, ivOrNonce)
			blockMode.XORKeyStream(cipherText, cipherText)
		}
	case "GCM":
		{
			aesgcm, err := cipher.NewGCM(block)
			if err != nil {
				return nil, err
			}

			plaintext, err := aesgcm.Open(nil, ivOrNonce, cipherText, nil)
			if err != nil {
				return nil, err
			} else {
				plaintext = padding.Unpadding(plaintext, a.padding)
				return plaintext, nil
			}

		}
	}

	cipherText = padding.Unpadding(cipherText, a.padding)

	return cipherText, nil
}
