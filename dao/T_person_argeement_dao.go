package dao

import (
	"git.forms.io/isaving/models"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_person_argeement struct {
	AgreementId     string  `xorm:"'agreement_id' pk"`
	AgreementType   string  `xorm:"'agreement_type'"`
	Currency        string  `xorm:"'currency'"`
	CashtranFlag    string  `xorm:"'cashtran_Flag'"`
	AgreementStatus string  `xorm:"'agreement_status'"`
	FreezeType      string  `xorm:"'freeze_type'"`
	TermFlag        string  `xorm:"'term_flag'"`
	DepcreFlag      string  `xorm:"'depcre_flag'"`
	AccFlag         string  `xorm:"'acc_flag'"`
	AccAcount       string  `xorm:"'acc_acount'"`
	BegnTxDt        string  `xorm:"'begn_tx_dt'"`
	AppntDtFlg      string  `xorm:"'appnt_dt_flg'"`
	AppntDt         string  `xorm:"'appnt_dt'"`
	PrductId        string  `xorm:"'prduct_id'"`
	PrductNm        int     `xorm:"'prduct_nm'"`
	CstmrId         string  `xorm:"'cstmr_id'"`
	CstmrTyp        string  `xorm:"'cstmr_typ'"`
	AccuntNme       string  `xorm:"'accunt_nme'"`
	UsgCod          string  `xorm:"'usg_cod'"`
	WdrwlMthd       string  `xorm:"'wdrwl_mthd'"`
	AccPsw          string  `xorm:"'acc_psw'"`
	PswWrongTime    int     `xorm:"'psw_wrong_time'"`
	TrnWdrwmThd     string  `xorm:"'trn_wdrwm_thd'"`
	OpnAmt          float64 `xorm:"'opn_amt'"`
	AccCnclFlg      string  `xorm:"'acc_cncl_flg'"`
	AutCnl          string  `xorm:"'aut_cnl'"`
	DytoCncl        int     `xorm:"'dyto_cncl'"`
	SttmntFlg       string  `xorm:"'sttmnt_flg'"`
	WthdrwlMthd     string  `xorm:"'wthdrwl_mthd'"`
	OvrDrwFlg       string  `xorm:"'ovr_drw_flg'"`
	CstmrCntctAdd   string  `xorm:"'cstmr_cntct_add'"`
	CstmrCntctPh    string  `xorm:"'cstmr_cntct_ph'"`
	CstmrCntctEm    string  `xorm:"'cstmr_cntct_Em'"`
	BnCrtAcc        string  `xorm:"'bn_crt_acc'"`
	BnAcc           string  `xorm:"'bn_acc'"`
	AccOpnDt        string  `xorm:"'acc_opn_dt'"`
	AccOpnEm        string  `xorm:"'acc_opn_em'"`
	AccCanclDt      string  `xorm:"'acc_cancl_dt'"`
	AccCanclEm      string  `xorm:"'acc_cancl_em'"`
	AccCanclResn    string  `xorm:"'acc_cancl_resn'"`
	LastUpDatetime  string  `xorm:"'last_up_datetime'"`
	LastUpBn        string  `xorm:"'last_up_bn'"`
	LastUpEm        string  `xorm:"'last_up_em'"`
	TccStatus       string  `xorm:"'tcc_status'"`
}

func (t *T_person_argeement) TableName() string {
	return "t_person_argeement"
}

func QueryTpersonargeementById(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_person_argeement *T_person_argeement, err error) {
	t_person_argeement = &T_person_argeement{
		AgreementId: sv9000008I.FezAgrmt,
	}
	log.Debug("Query where：", t_person_argeement)
	exists, err := o.Get(t_person_argeement)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_person_argeement, nil
}

func UpdateTpersonargeementById(AgreementId string, FreezeType string, o *xorm.Engine) error {
	sql := "update t_person_argeement set freeze_type =? where agreement_id =?"
	_, err := o.Exec(sql, FreezeType, AgreementId)
	if err != nil {
		log.Info("Update t_person_argeement failed")
		return err
	}
	return nil
}
