package dao

import (
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	"github.com/xormplus/xorm"
	"time"
)

var EngineCache *xorm.Engine

// @Desc Initialize database information
func InitDatabase() error {
	addr := beego.AppConfig.String("mysql::addr")
	userName := beego.AppConfig.String("mysql::user")
	password := beego.AppConfig.String("mysql::password")
	dbName := beego.AppConfig.String("mysql::database")

	dsn := userName + ":" + password + "@tcp(" + addr + ")/" + dbName
	engine, err := xorm.NewEngine("mysql", dsn)

	if err != nil {
		return err
	}

	maxIdleConns := beego.AppConfig.DefaultInt("mysql::maxIdleConns", 30)
	maxOpenConns := beego.AppConfig.DefaultInt("mysql::maxOpenConns", 30)
	maxLifeValue := beego.AppConfig.DefaultInt("mysql::maxLifeValue", 540)
	showSql := beego.AppConfig.DefaultBool("mysql::showSql", false)

	engine.SetMaxIdleConns(maxIdleConns)
	engine.SetMaxOpenConns(maxOpenConns)
	engine.SetConnMaxLifetime(time.Duration(maxLifeValue)*time.Second)
	engine.ShowSQL(showSql)

	EngineCache = engine
	return nil
}