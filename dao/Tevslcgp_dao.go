package dao

import (
	"git.forms.io/isaving/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_evslcgp struct {
	FezBusNm       string  `xorm:"'fez_bus_nm' pk"`    //冻结业务编号
	FezType        string  `xorm:"'fez_type'"`         //冻结方式
	EffctivDate    string  `xorm:"'effctiv_date'"`     //冻结生效日
	FezDueDate     string  `xorm:"'fez_due_date'"`     //冻结到期日
	FezDate        string  `xorm:"'fez_date'"`         //冻结日期
	FezTime        string  `xorm:"'fez_time'"`         //冻结时间
	FezMech        string  `xorm:"'fez_mech'"`         //冻结行所
	FezEm          string  `xorm:"'fez_em'"`           //冻结柜员
	FezAuthEm      string  `xorm:"'fez_auth_em'"`      //冻结授权柜员
	FezMechType    string  `xorm:"'fez_mech_type'"`    //冻结发起方
	FezMechName    string  `xorm:"'fez_mech_name'"`    //冻结发起方名称
	FezMode        string  `xorm:"'fez_mode'"`         //冻结方式
	CstmrId        string  `xorm:"'cstmr_id'"`         //冻结客户编号
	CstmrTyp       string  `xorm:"'cstmr_typ'"`        //冻结客户类型
	MdsTyp         string  `xorm:"'mds_typ'"`          //介质类型
	MdsNm          string  `xorm:"'mds_nm'"`           //介质号码
	FezAgrmt       string  `xorm:"'fez_agrmt'"`        //冻结合约号
	FezAgrmtTyp    string  `xorm:"'fez_agrmt_typ'"`    //冻结合约类型
	FezCur         string  `xorm:"'fez_cur'"`          //币别
	FezAmt         float64 `xorm:"'fez_amt'"`          //冻结金额
	FezDocnm       string  `xorm:"'fez_docnm'"`        //冻结文书号
	FezReason      string  `xorm:"'fez_reason'"`       //冻结原因
	FficialsName1  string  `xorm:"'fficials_name1'"`   //执法人员名称1
	FficialsId1    string  `xorm:"'fficials_Id1'"`     //执法人员证件号码1
	FficialsName2  string  `xorm:"'fficials_name2'"`   //执法人员名称2
	FficialsId2    string  `xorm:"'fficials_Id2'"`     //执法人员证件号码2
	UnfezFwNm      int     `xorm:"'unfez_fw_nm'"`      //解冻优先次序
	FezStatus      string  `xorm:"'fez_status'"`       //冻结生效标志 Y-effect；N-no effect
	BnkFezTpy      string  `xorm:"'bnk_fez_tpy'"`      //银行冻结区分 M-Internal management;T-tansation
	LastUpDatetime string  `xorm:"'last_up_datetime'"` //最后更新日期时间
	LastUpBn       string  `xorm:"'last_up_bn'"`       //最后更新行所
	LastUpEm       string  `xorm:"'last_up_em'"`       //最后更新柜员
	TccStatus      int     `xorm:"'tcc_status'"`
}

func (t *T_evslcgp) TableName() string {
	return "t_evslcgp"
}

func InsertTevslcgpOne(t_evslcgp *T_evslcgp, o *xorm.Engine) error {
	num, err := o.Insert(t_evslcgp)
	if err != nil {
		return err
	}
	if num != 1 {
		return errors.New("Insert tevslcgp record to database failed", "")
	}
	return nil
}

// update tcc_status=0
func UpdateTevslcgpById(FezBusNm string, o *xorm.Engine) error {
	sql := "update t_evslcgp set tcc_status =0 where fez_bus_nm =?"
	_, err := o.Exec(sql, FezBusNm)
	if err != nil {
		log.Info("Update t_evslcgp failed")
		return err
	}
	return nil
}

func DeleteTevslcgpById(FezBusNm string, o *xorm.Engine) error {
	sql := "delete from t_evslcgp where tcc_status = 1 and fez_bus_nm =?"
	_, err := o.Exec(sql, FezBusNm)
	if err != nil {
		log.Info("delete T_evslcgp failed")
		return err
	}
	return nil
}

func QueryTevslcgpByFezAgrmt(sv9000008I *models.SSV9000008I, o *xorm.Engine) (result []map[string]string, err error) {
	sql := "select unfez_fw_nm UnfezFwNm from t_evslcgp where fez_agrmt=? and fez_mech_type=? and tcc_status=0 order by unfez_fw_nm desc limit 1 "
	results, err := o.QueryString(sql, sv9000008I.FezAgrmt, sv9000008I.FezMechType)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if len(results) == 0 {
		log.Info("Record is empty")
		return nil, nil
	}
	return results, nil
}

func QueryTevslcgpByFezDocNm(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezMechType: sv9000008I.FezMechType,
		FezDocnm:    sv9000008I.FezDocNm,
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

func QueryTevslcgpByFezApNm(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezMechType: sv9000008I.FezMechType,
		FezDocnm:    sv9000008I.FezApNm,
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

//合约号+合约类型+未生效+未解冻 / 合约号+合约类型+权力机关+未生效+未解冻
func QueryTevslcgpByFezStatusOrMechTypeFezSta(sv9000008I *models.SSV9000008I, FezMechType string, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezStatus:   "N", //Y-effect；N-no effect
		FezMechType: FezMechType,
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Where("fez_type!=?", "0").Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

//合约号+合约类型+权力机构+状态是1-账户冻结/3-暂禁
func QueryTevslcgpByFezType(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezMechType: sv9000008I.FezMechType,
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Where("fez_type=? or fez_type=?", "1", "3").Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

//合约号+合约类型+银行内部+生效+状态1-账户状态/3-暂禁查冻结信息表
func QueryTevslcgpByFezMechType_2(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezMechType: "2",
		FezStatus:   "Y",
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Where("fez_type=? or fez_type=?", "1", "3").Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

/*//合约号+合约类型+银行内部+生效+状态1-账户状态/3-暂禁 置无效
func UpdateTevslcgpByFezMechType_2(FezAgrmt string, FezAgrmtTyp string, o *xorm.Engine) error {
	sql := "update t_evslcgp set fez_status ='N' where fez_agrmt =? and fez_agrmt_typ=? and fez_mech_type='2' and fez_status='Y' and fez_type='1' or fez_type='3'"
	_, err := o.Exec(sql, FezAgrmt, FezAgrmtTyp)
	if err != nil {
		log.Info("Update t_evslcgp failed")
		return err
	}
	return nil
}
*/
//合约号+合约类型+权力机构+不生效+状态是2-金额冻结
func QueryTevslcgpByMechTypeFezSta_NFezM_2(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezStatus:   "N", //Y-effect；N-no effect
		FezMechType: sv9000008I.FezMechType,
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Where("fez_type=?", "2").Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}

//合约号+合约类型+银行内部+生效+状体是2-金额冻结 按解冻优先次序的降序排序查询冻结合约表
func QueryTevslcgpByFezMechType_2FType_2(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp []*T_evslcgp, err error) {
	t_evslcgp1 := &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezMechType: "2",
		FezStatus:   "Y",
		FezType:     "2",
	}
	log.Debug("Query where：", t_evslcgp)
	err = o.Desc("unfez_fw_nm").Find(&t_evslcgp, t_evslcgp1)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	return t_evslcgp, nil
}

//合约号+合约类型+生效+银行内部+状体是2-金额冻结 置无效 / 合约号+合约类型+银行内部+生效+状态1-账户状态/3-暂禁 置无效
func UpdateTevslcgpByFezMechType_2FType_2(FezBusNm string, o *xorm.Engine) error {
	sql := "update t_evslcgp set fez_status ='N' where fez_bus_nm=?"
	_, err := o.Exec(sql, FezBusNm)
	if err != nil {
		log.Info("Update t_evslcgp failed")
		return err
	}
	return nil
}

//合约号+合约类型+权力机构+未生效+状态暂禁/账户冻结
func QueryTevslcgpByFezType_1N(sv9000008I *models.SSV9000008I, o *xorm.Engine) (t_evslcgp *T_evslcgp, err error) {
	t_evslcgp = &T_evslcgp{
		FezAgrmt:    sv9000008I.FezAgrmt,
		FezAgrmtTyp: sv9000008I.FezAgrmtTyp,
		FezMechType: "1",
		FezStatus:   "N",
	}
	log.Debug("Query where：", t_evslcgp)
	exists, err := o.Where("fez_type=? or fez_type=?", "1", "3").Get(t_evslcgp)
	if err != nil {
		log.Info("The information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evslcgp, nil
}
